package main

import (
	"log"

	"github.com/go-openapi/loads"

	"go-articles-sbercode-spring-2021/internal/generated/restapi"
	"go-articles-sbercode-spring-2021/internal/generated/restapi/operations"
	"go-articles-sbercode-spring-2021/internal/handlers"
)

func main() {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			log.Fatalln(err)
		}
	}()

	server.Port = 8083

	api.CheckHealthHandler = operations.CheckHealthHandlerFunc(handlers.HealthCheckHandler)
	api.CreateArticleHandler = operations.CreateArticleHandlerFunc(handlers.CreateArticleHandler)
	api.GetArticleHandler = operations.GetArticleHandlerFunc(handlers.GetArticleHandler)

	//connector.NewDBConnector(false)

	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
