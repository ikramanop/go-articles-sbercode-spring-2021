package methods

import (
	"log"

	"github.com/go-pg/pg/v10"

	"go-articles-sbercode-spring-2021/internal/database/models"
)

func GetArticleByIdentifier(db *pg.DB, articleIdentifier string) (user *models.Article, err error) {
	user = new(models.Article)
	err = db.Model(user).
		Where("article_identifier = ?", articleIdentifier).
		Relation("Text").
		Relation("Media").
		Relation("Comment").
		Select()

	if err != nil {
		if err.Error() == "pg: no rows in result set" {
			return nil, nil
		} else {
			log.Printf("FAIL. Fatal error with get posts info: %s", err)

			return nil, err
		}
	}

	return user, nil
}
