package methods

import (
	"fmt"
	"go-articles-sbercode-spring-2021/internal/database/models"

	"github.com/go-pg/pg/v10"

	"go-articles-sbercode-spring-2021/internal/generated/restapi/operations"
)

type Id struct {
	ID int64 `pg:"id"`
}

func CreateArticle(db *pg.DB, body operations.CreateArticleBody) (string, error) {
	id := Id{}
	_, err := db.Query(&id, "select last_value + 1 id from article_id_seq;")
	if err != nil {
		return "", err
	}

	article := &models.Article{}
	texts := make([]*models.Text, 0)
	medias := make([]*models.Media, 0)

	for _, text := range body.Content.Text {
		texts = append(texts, &models.Text{
			ArticleID: id.ID,
			Count:     text.Count,
			Types:     text.Types,
			Data:      text.Data,
		})
	}

	for _, media := range body.Content.Media {
		medias = append(medias, &models.Media{
			ArticleID: id.ID,
			Count:     media.Count,
			Types:     media.Types,
			Data:      media.Data,
		})
	}

	uuid := fmt.Sprintf("article.%d", id.ID)
	article.Text = texts
	article.Media = medias
	article.ArticleIdentifier = uuid
	article.CreatedBy = body.CreatedBy
	article.ContentCount = body.Content.Count

	_, err = db.Model(article).Insert()
	if err != nil {
		return "", err
	}
	_, err = db.Model(&texts).Insert()
	if err != nil {
		return "", err
	}
	_, err = db.Model(&medias).Insert()
	if err != nil {
		return "", err
	}

	return uuid, nil
}
