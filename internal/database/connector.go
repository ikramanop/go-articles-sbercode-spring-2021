package connector

import (
	"fmt"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"

	"go-articles-sbercode-spring-2021/internal/database/models"
)

func NewDBConnector(isDBCreated bool) *pg.DB {
	db := pg.Connect(&pg.Options{
		Addr:     "82.146.61.94:5432",
		User:     "root",
		Password: "root",
		Database: "articles",
	})

	if !isDBCreated {
		err := createSchema(db)
		if err != nil {
			panic(fmt.Errorf("FAIL. Fatal error with db connector: %s", err))
		}
	}

	return db
}

func createSchema(db *pg.DB) error {
	modelsInit := []interface{}{
		(*models.Article)(nil),
		(*models.Text)(nil),
		(*models.Media)(nil),
		(*models.Comment)(nil),
	}

	for _, model := range modelsInit {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: false,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
