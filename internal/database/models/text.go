package models

type Text struct {
	tableName struct{} `pg:"text"`
	ID        uint64   `pg:",pk"`
	ArticleID int64    `pg:",use_zero"`
	Count     int64    `pg:",use_zero"`
	Types     string
	Data      string
}
