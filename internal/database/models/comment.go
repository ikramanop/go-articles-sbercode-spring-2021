package models

import "time"

type Comment struct {
	tableName struct{} `pg:"comment"`
	ID        int64    `pg:",pk"`
	ArticleID int64
	CreatedBy string
	Data      string
	Rating    int64     `pg:"default:0,use_zero"`
	CreatedAt time.Time `pg:"default:now()"`
}
