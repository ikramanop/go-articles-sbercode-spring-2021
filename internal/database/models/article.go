package models

import "time"

type Article struct {
	tableName         struct{} `pg:"article"`
	ID                int64    `pg:",pk"`
	ArticleIdentifier string
	CreatedBy         string
	ContentCount      int64      `pg:",use_zero"`
	Rating            int64      `pg:"default:0,use_zero"`
	CreatedAt         time.Time  `pg:"default:now()"`
	Text              []*Text    `pg:"rel:has-many"`
	Media             []*Media   `pg:"rel:has-many"`
	Comment           []*Comment `pg:"rel:has-many"`
}
