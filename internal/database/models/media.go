package models

type Media struct {
	tableName struct{} `pg:"media"`
	ID        uint64   `pg:",pk"`
	ArticleID int64   `pg:",use_zero"`
	Count     int64    `pg:",use_zero"`
	Types     string
	Data      []byte
}
