package handlers

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"go-articles-sbercode-spring-2021/internal/database"
	"go-articles-sbercode-spring-2021/internal/database/methods"
	"go-articles-sbercode-spring-2021/internal/generated/restapi/operations"
)

func CreateArticleHandler(params operations.CreateArticleParams) middleware.Responder {
	log.Printf("Hit POST /articles from %s\n", params.HTTPRequest.UserAgent())

	db := connector.NewDBConnector(true)
	defer db.Close()

	identifier, err := methods.CreateArticle(db, params.ArticleData)
	if err != nil {
		log.Println(err)

		log.Println("Error creating article")

		return operations.NewCreateArticleBadRequest().WithPayload(&operations.CreateArticleBadRequestBody{
			ErrorCode: 11,
			Status:    false,
		})
	}

	log.Printf("Article with identifier %s was successfully created", identifier)
	return operations.NewCreateArticleOK().WithPayload(&operations.CreateArticleOKBody{
		ErrorCode:  0,
		Status:     true,
		Identifier: identifier,
	})
}
