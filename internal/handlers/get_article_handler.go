package handlers

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"go-articles-sbercode-spring-2021/internal/database"
	"go-articles-sbercode-spring-2021/internal/database/methods"
	"go-articles-sbercode-spring-2021/internal/generated/restapi/operations"
)

func GetArticleHandler(params operations.GetArticleParams) middleware.Responder {
	log.Printf("Hit GET /article/%s from %s\n", params.ArticleIdentifier, params.HTTPRequest.UserAgent())

	db := connector.NewDBConnector(true)
	defer db.Close()

	article, err := methods.GetArticleByIdentifier(db, params.ArticleIdentifier)
	if err != nil {
		log.Println(err)

		log.Printf("Error returning User with identifier %s", params.ArticleIdentifier)

		return operations.NewGetArticleBadRequest().WithPayload(&operations.GetArticleBadRequestBody{
			ErrorCode: 12,
			Status:    false,
		})
	}

	if article == nil {
		log.Printf("No User with identifier %s", params.ArticleIdentifier)

		return operations.NewGetArticleBadRequest().WithPayload(&operations.GetArticleBadRequestBody{
			ErrorCode: 13,
			Status:    false,
		})
	}

	texts := make([]*operations.GetArticleOKBodyContentTextItems0, 0)
	medias := make([]*operations.GetArticleOKBodyContentMediaItems0, 0)
	comments := make([]*operations.GetArticleOKBodyContentCommentItems0, 0)
	for _, text := range article.Text {
		texts = append(texts, &operations.GetArticleOKBodyContentTextItems0{
			Count: text.Count,
			Data:  text.Data,
			Types: text.Types,
		})
	}
	for _, media := range article.Media {
		medias = append(medias, &operations.GetArticleOKBodyContentMediaItems0{
			Count: media.Count,
			Data:  media.Data,
			Types: media.Types,
		})
	}
	for _, comment := range article.Comment {
		comments = append(comments, &operations.GetArticleOKBodyContentCommentItems0{
			CreatedAt: comment.CreatedAt.String(),
			CreatedBy: comment.CreatedBy,
			Data:      comment.Data,
			Rating:    comment.Rating,
		})
	}

	log.Printf("Successfully returned User with identifier %s", params.ArticleIdentifier)

	return operations.NewGetArticleOK().WithPayload(&operations.GetArticleOKBody{
		ArticleIdentifier: article.ArticleIdentifier,
		Content: &operations.GetArticleOKBodyContent{
			ContentID: article.ID,
			Count:     article.ContentCount,
			Media:     medias,
			Text:      texts,
			Comment:   comments,
		},
		CreatedAt: article.CreatedAt.String(),
		CreatedBy: article.CreatedBy,
	})
}
