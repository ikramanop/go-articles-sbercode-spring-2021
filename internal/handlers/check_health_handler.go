package handlers

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"go-articles-sbercode-spring-2021/internal/generated/restapi/operations"
)

func HealthCheckHandler(params operations.CheckHealthParams) middleware.Responder {
	log.Printf("Hit GET /health/check from %s\n", params.HTTPRequest.UserAgent())

	return operations.NewCheckHealthOK().WithPayload(
		&operations.CheckHealthOKBody{
			ErrorCode: 0,
			Status:    true,
		},
	)
}
